
AutocompleteItem = React.createClass
    render: ()->
        (React.DOM.li {}, this.props.name)


AutocompleteBox = React.createClass
    render: ()->
        nodes = @props.list.map (i)->
            return (AutocompleteItem {name : i})

        (React.DOM.div {className: "autocomplete"},
            React.DOM.ul {},  nodes
        )



CSFDApp = React.createClass

    getInitialState: ()->
        return {list: [], timeout: null}

    request: (term)->

        clearTimeout(@state.timeout)

        setTimeout ()=>
            $.getJSON("http://csfdapi.cz/movie?search=" + encodeURIComponent(term), (data)=>
                @setState list: data.map (item)->
                    return item.names.cs+"( "+item.year+")"
            )
            return
        , 300
        return


    onKeyUp: (e)->
        term = e.target.value
        if term.length <= 3
            return

        this.request(term)

    render: ()->
        (React.DOM.div {className: "box"},
            React.DOM.input {type: "text", onKeyUp: @onKeyUp}
            AutocompleteBox {list: this.state.list}
        )



React.renderComponent (CSFDApp {}), document.body
