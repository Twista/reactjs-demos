###
    Data table
###
DataTable = React.createClass
    displayName: "DataTable"

    sort: {
        asc:  (a, b)-> (a > b)
        desc: (a, b)-> (a < b)
    }

    getInitialState: ()->
        {rows : [], cols: [], perPage: 10, activePage: 1, showBy: []}

    componentWillReceiveProps: (properties)->
        @setState {
            perPage: properties.perPage
        }

    componentDidMount: ()->
        $.getJSON @props.url, (data)=>
            @setState {
                rows: data.rows
                cols: data.cols
            }

    onPagerClick: (index)->
        @setState {activePage: index}

    onShowChange: (e)->
        @setState {perPage: e.target.value, activePage: 1}


    onSortClick: (key, sortType)->
        if sortType == "asc"
            sortFunc = @sort.asc
        else
            sortFunc = @sort.desc

        rows =  @state.rows.sort (a, b) ->
            if a[key] == b[key]
                return 0
            if  sortFunc(a[key], b[key])
                return 1

            return -1
        @setState {rows: rows}



    render: ()->
        # count min and max - records which should be displayed on page
        max = (@state.activePage * @state.perPage)
        min = max - @state.perPage
        max = @state.rows.length if (max > @state.rows.length)

        # slice rows by min and max
        rows = @state.rows.slice(min, max)

        React.DOM.div {}, [
            (TablePager({key: "upper", total: @state.rows.length, perPage: @state.perPage, onClickHandler: @onPagerClick, active: @state.activePage}))
            (ShowOnPage({items: @props.showBy, perPage: @state.perPage, onShowChange: @onShowChange }))
            React.DOM.table({className: "table"}, [
                (TableHead({cols: @state.cols, sortByClickHandler: @onSortClick}))
                (TableBody({cols: @state.cols, rows: rows}))
            ])
            (TablePager({key: "bottom", total: @state.rows.length, perPage: @state.perPage, onClickHandler: @onPagerClick, active: @state.activePage}))
        ]

###
    Table Head
###
TableHead = React.createClass
    render: ()->
        cols = this.props.cols.map (item)=>
            return React.DOM.th {key: item.key}, [
                item.label
                React.DOM.a {onClick: @props.sortByClickHandler.bind null, item.key, "asc"},
                    React.DOM.span {className: "glyphicon glyphicon-chevron-up"}
                React.DOM.a {onClick: @props.sortByClickHandler.bind null, item.key, "desc"},
                    React.DOM.span {className: "glyphicon glyphicon-chevron-down"}
            ]

        React.DOM.thead {},
            cols

###
    Table Body
###
TableBody = React.createClass

    # value formaters
    formaters : {
        int:   (val)-> val
        str:   (val)-> val
        float: (val)-> val.toFixed(2)
    }

    render: ()->
        lines = this.props.rows.map (rows)=>
            React.DOM.tr {}, this.props.cols.map (col)=>
                React.DOM.td {}, @formaters[col.type](rows[col.key])

        React.DOM.tbody {}, lines


ShowOnPage = React.createClass
    displayName: "ShowOnPage"

    render: ()->
        lines = @props.items.map (item)=>
            return React.DOM.option {selected: (@props.perPage == item)}, item

        React.DOM.select {name: "showOnPage", className: "form-control", value: @props.perPage, onChange: @props.onShowChange},
            lines



###
    Table Pager
###
TablePager = React.createClass
    displayName: "Pager"

    render: ()->
        totalPages = Math.ceil(@props.total / @props.perPage)

        pages = []
        for i in [1..totalPages]
            pages.push React.DOM.li {key: i, className: "active" if @props.active == i},
                React.DOM.a {onClick: @props.onClickHandler.bind null, i}, i

        React.DOM.ul {className: "pagination"},
            pages



sourceUrl = "http://www.sportshacker.net/data/hello_world_react/teams.json"
React.renderComponent(DataTable({url: sourceUrl, perPage: 10, showBy: [10,20,30]}), document.getElementById("container"))